package es.palacios;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Piramide extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Piramide");
        GridPane gridPane = new GridPane();

        gridPane.setHgap(5);
        gridPane.setVgap(5);

        int center = 4;
        for (int i = 0; i <= 4; i++) {
            int range = (1 + (2 * i));
            int column = center - range/2;

            for (int j = 0; j < range; j++) {
                Circle circle = createCircle();
                gridPane.add(circle,column+j, i);
            }
        }

        Scene scene = new Scene(gridPane,430,280);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

    public static Circle createCircle (){
        Circle circle = new Circle(20);
        circle.setFill(Color.rgb(31,250,15));
        circle.setStrokeWidth(3);
        circle.setStroke(Color.rgb(164,15,250));
        return circle;
    }
}
